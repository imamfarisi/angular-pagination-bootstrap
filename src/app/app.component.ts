import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  data = [
    { no: 1, name: 'A', address: 'Jln. A Raya' },
    { no: 2, name: 'B', address: 'Jln. B Raya' },
    { no: 3, name: 'C', address: 'Jln. C Raya' },
    { no: 4, name: 'D', address: 'Jln. D Raya' },
    { no: 5, name: 'E', address: 'Jln. E Raya' },
    { no: 6, name: 'F', address: 'Jln. F Raya' },
    { no: 7, name: 'G', address: 'Jln. G Raya' },
    { no: 8, name: 'H', address: 'Jln. H Raya' },
    { no: 9, name: 'I', address: 'Jln. I Raya' },
    { no: 10, name: 'J', address: 'Jln. J Raya' },
    { no: 11, name: 'K', address: 'Jln. K Raya' },
    { no: 12, name: 'L', address: 'Jln. L Raya' },
    { no: 13, name: 'M', address: 'Jln. M Raya' },
    { no: 14, name: 'N', address: 'Jln. N Raya' },
    { no: 15, name: 'O', address: 'Jln. O Raya' },
    { no: 16, name: 'P', address: 'Jln. P Raya' },
    { no: 17, name: 'Q', address: 'Jln. Q Raya' },
    { no: 18, name: 'R', address: 'Jln. R Raya' },
    { no: 19, name: 'S', address: 'Jln. S Raya' },
    { no: 20, name: 'T', address: 'Jln. T Raya' },
    { no: 21, name: 'U', address: 'Jln. U Raya' },
    { no: 22, name: 'V', address: 'Jln. V Raya' },
  ]

  dataPagination: typeof this.data = []
  nextNumber : number = 0

}

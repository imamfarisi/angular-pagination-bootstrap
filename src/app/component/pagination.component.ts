import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";

@Component({
    selector: 'app-pagination',
    template: `
<nav aria-label="Page navigation example" *ngIf="totalPage > 1">
    <ul class="pagination justify-content-center">
        <li class="page-item" *ngIf="startIndexPage != 0">
            <a class="page-link" style="cursor: pointer;" (click)="onForward()" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        <li *ngFor="let d of shownPages; let i=index;" class="page-item {{d}}"><a style="cursor: pointer;" class="page-link" (click)="clickPage(i+1)">{{i+1}}</a></li>
        <li class="page-item" *ngIf="this.currentPage != this.totalShowPerPage.length">
            <a style="cursor: pointer;" class="page-link" (click)="onNext()" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>`
})
export class PaginationComponent implements OnChanges {

    @Input()
    data: any[] = []

    @Input()
    dataPagination: any[] = []

    @Output()
    dataPaginationChange = new EventEmitter<any[]>()

    @Input()
    nextNumber: number = 0

    @Output()
    nextNumberChange = new EventEmitter<number>()

    @Input()
    rows: number = 0

    active: string = 'active'

    totalPage: number = 5
    totalShowPerPage: number[] = []
    shownPages: string[] = []

    startIndexPage: number = 0
    currentPage: number = 1

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['data'] && this.data && this.data.length > 0) {
            this.totalShowPerPage = []
            this.shownPages = []
            this.currentPage = 1
            this.startIndexPage = 0
            Promise.resolve().then(() => this.nextNumberChange.emit(0))

            this.initData()
            this.initPerPage()
        }
    }

    initPerPage(): void {
        if (this.totalShowPerPage.length < 1) {
            for (let i = 1; i <= Math.ceil(this.data.length / this.rows); i++) {
                this.totalShowPerPage.push(i)
            }

            if (this.totalPage > this.totalShowPerPage.length) {
                this.totalPage = this.totalShowPerPage.length
            }
        }

        if (this.shownPages.length < 1) {
            for (let i = 1; i <= this.totalPage; i++) {
                this.shownPages.push(i == 1 ? 'active' : '')
            }
        }
    }

    initData(): void {
        const dataPagination = this.data.slice(this.startIndexPage, this.startIndexPage + this.rows)
        Promise.resolve().then(() => this.dataPaginationChange.emit(dataPagination))
    }

    clickPage(i: number): void {
        this.shownPages[this.currentPage - 1] = ''
        this.currentPage = i
        this.startIndexPage = ((i * this.rows) - this.rows)
        this.setActivePage(i)
        this.nextNumberChange.emit((this.currentPage * this.rows) - this.rows)
    }

    setActivePage(i: number): void {
        this.shownPages[i - 1] = 'active'
        this.initData()
    }

    onNext(): void {
        this.clickPage(this.currentPage + 1)
    }

    onForward(): void {
        this.clickPage(this.currentPage - 1)
    }

}